# Base image
FROM node:14.16.0-alpine3.10

# Set working directory in container
WORKDIR /app

# Copy package.json to WORKDIR
COPY package.json ./

# Install dependencies
RUN yarn install --production --frozen-lockfile

# Copy app files to container
COPY . .

# Build production version of the app
RUN yarn build

# Serve the app using Nginx web server
FROM nginx:1.21.0-alpine
COPY --from=0 /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]