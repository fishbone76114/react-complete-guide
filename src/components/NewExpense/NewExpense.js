import { useState } from 'react';
import ExpenseForm from './ExpenseForm';
import './NewExpense.css'

const NewExpense = (props) => {
    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData);
    };

    const [isAddExpense, setIsAddExpense] = useState(false)

    const startAddExpenseHanlder = () => {
        setIsAddExpense(true);
    }

    const stopAddExpenseHanlder = () => {
        setIsAddExpense(false);
    }

    return (
        <div className="new-expense">
            {!isAddExpense && <button onClick={startAddExpenseHanlder}>Add new Expense</button>}
            {isAddExpense && <ExpenseForm onCancel={stopAddExpenseHanlder} onSaveExpenseData={saveExpenseDataHandler} />}
        </div>
    )
}

export default NewExpense;